import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormLoginComponent } from './views/form-login/form-login.component';
import { DefaultPageComponent } from './views/default-page/default-page.component';

const routes: Routes = [
  {
    path: '',
    component: DefaultPageComponent
  },
  {
    path: 'login',
    component: FormLoginComponent,
    data: {
      title: 'Авторизация'
   }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
